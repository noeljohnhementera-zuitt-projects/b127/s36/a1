const express = require("express");
const router = express.Router();
const courseControllers = require("../controllers/courseControllers");
const auth = require("../auth");

// Route for creating a course
// Base url, localhost:4000/courses/
router.post("/", auth.verify, (req, res) =>{ 		// we need auth verify if included sya sa data variable sa auth.js
	
	// Check if user is admin or not
	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin	// we use auth.decode dahil kasama sa token (auth.js) ang isAdmin
	}
	console.log(data)					// We added this one para lumabas sa terminal to determine if isAdmin = true

	courseControllers.addCourse(data)	// data yung variable
	.then(result =>{					
		res.send(result);
	})
})


// Retrieve all courses
router.get("/all", (req, res)=>{
	courseControllers.getAllCourses()
	.then(result =>{
		res.send(result);
	})
})

// Retrieve all true courses
router.get("/", (req, res)=>{
	courseControllers.getAllActive()
	.then(result =>{
		res.send(result);
	})
})

// Retrieve specific course
router.get("/:id", (req,res)=>{
	courseControllers.specificCourse(req.params.id)
	.then(result =>{
		res.send(result)
	})
})

// Update a course
/*router.put("/:id", auth.verify, (req, res)=>{

	const data = {					// kinukuha ang isAdmin
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	console.log(data)
		// course: req.body,
		// id: req.params

	courseControllers.updateCourse(data, req.body, req.params.id)
	.then(result=>{
		res.send(result)
	})
})*/

// Sir Paulo's code
router.put('/:courseId', auth.verify, (req, res) => {
    const data = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }

    if (data.isAdmin) {
        courseControllers.updatedCourse(req.params, req.body) // hindi na ilalagay ang isAdmin dahil kasama na sa statement
        .then(result => 
        	res.send(result));
    }else{
        res.send(false)
    }
      
})

// Archive or Soft Delete a Course
router.put("/:courseId/archive", auth.verify, (req, res)=>{			// auto.verify dahil si IsAdmin lang ang magdelete
	
	const date = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		courseControllers.archiveCourse(req.params)
			.then(result =>
				res.send(result))
	}else{
		res.send(false)
	}
	
})				

// Alternative
router.put('/:courseId/archive', auth.verify, (req, res) => {
	const date = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
	courseController.archiveCourse(req.params, req.body).then(result => res.send(result))
	}else{
		res.send(false)
	}
})



module.exports = router;