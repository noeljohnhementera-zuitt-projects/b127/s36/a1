const Course = require("../models/Course");

/*
Creation of Course
Business Logic
1. Create a condtional statement that will check if the user is an admin
2. Create a course object using the mongoose model and the information from the request body and the id
	// from the header
3. Save the new course to the database
*/

module.exports.addCourse = (data) =>{
	// if user is an admin
	if(data.isAdmin){							// tanggalin muna ang validation (if, else) dahil may server error
		let newCourse = new Course({
			name: data.course.name,					// data is the parameter
			description:data.course.description,	// course is found in data variable under controllers
			price: data.course.price
		})
		return newCourse.save()
		.then((course, error) =>{
			// course creation faile
			if(error){
				return false;
			}else{
				return true
			}
		})

	}else{
		return false;
	}
}

// Retrieve all courses
module.exports.getAllCourses = () =>{			// hindi na kailangan ng authorization or authentication
	return Course.find({})
	.then(result =>{
		return result;
	})
}

// Retrieve all true courses
module.exports.getAllActive = () =>{			// hindi na kailangan ng authorization or authentication
	return Course.find( {isActive: true} )
}

// Retrieve specific course
module.exports.specificCourse = (courseId) =>{
	return Course.findById(courseId)
}


// Update a course
/*
1. Check if the user is an admin
2. Create a variable which will contain the information retrieved from the req.body
3. Find and update the course using the courseId retrieved from the req.params property and the
	// variable containing the information form the request bdy
*/

// SUCCESSFUL Logic
// Update a course

/*module.exports.updateCourse = (data, courseNew, courseId) =>{
	return Course.findById(courseId)
	.then((res, err)=>{
		if(err){
			return false
		}else{
			if(data.isAdmin){
				res.name = courseNew.name
				res.description = courseNew.description
				res.price = courseNew.price
				return res.save()
				.then((result, error)=>{
					if(error){
						return false
					}else{
						return result
					}
				})
			}
		}
	})
}

// Sir Paulo's code
// update a course
module.exports.updateCourse = (req) => {
    return Course.findByIdAndUpdate({ _id: req.params.courseId }, 
        {   name: req.body.name, 
            description: req.body.description, 
            price: req.body.price 
        })
    .then(updatedCourse => { 
        return (updatedCourse) 
            ? { message: "Course update was successful", data:updatedCourse} 
            : { message: "Course update failed" }; })
    .catch(error => res.status(500).send({message: "Internal Server Error"}))
}*/

// Instructor's Controller
module.exports.updatedCourse = (reqParams, reqBody) =>{
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};
	// findByIdAndUpdate(ID, updates to be applied)
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse)
	.then((course, error)=>{
		// Course is not updated
		if(error){
			return false;
		// Course was updated
		}else{
			return true;
			// this will update the Schema sa MongoDB Atlas
		}
	})
}

/*
// Ternary operator (ES6 update)
if(error){
	return false
}else{
	return true
}

(error)
? = if() - false
: = else - true
*/

// Archive a course
/*
Business Logic
1. Check if admin(routes)
2. Create a variable where the isActive will change into false
3. Use findByIdAndUpdate(id, updatedVariable)
	3.1 Error handling

*/
module.exports.archiveCourse = (reqParams) =>{
	return Course.findByIdAndUpdate(reqParams.courseId)
	.then((archivedCourse, err)=>{
		if(err){
			return false
		}else{
			archivedCourse.isActive = false
			return archivedCourse
		}
	})
}

// Alternative
module.exports.archiveCourse = (reqParams, reqBody) => {
	let updatedCourse = {
		isActive: reqBody.isActive
	}
	return course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}