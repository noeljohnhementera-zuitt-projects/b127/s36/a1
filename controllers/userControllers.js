
// Activity at Line 104

const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");

const Course = require('../models/Course');
// New addition for to enroll a user

// Check if the email already exist
/*
Business model
1. Use mongoose "find" method to find duplicate emails
2. Use the .then method to send a response back to the client 
based on the result of the find method (email already exists / does not exist) 
*/

module.exports.checkEmailExists = (reqBody) => {
	return User.find( {email: reqBody.email} )
	.then(result =>{
		//The "find" method returns a record if a match is found
		if(result.length > 0){
			return true;
		}else{
			// No duplicate email found
			// The user is not yet registered in the database
			return false;
		}
	})
}	

//User registration
/*
Business Logic
1. Create a new user object using the mongoose model and the info from the request body
2. Error handling
	= if error, return error
	= else, save the new User to the database
*/

// This uses the information from the request body to provide all the necessary information
module.exports.registerUser = (reqBody) =>{
	let newUser = new User({
		firstName: reqBody.firstName,	// galing kay client
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		// 10 is the value provided as the number of "salt" rounds that the bcrypt algorithm will run
			// in order to encrypt the password
		password: bcrypt.hashSync(reqBody.password, 10) //reqBody.password
	})			  // this is used to hash or encrypt a password
	// Saves the created object to our database
	return newUser.save()
	.then((user, error)=>{
		// if user reg fails
		if(error){
			return false;
		}else{
			// User reg is successful
			return true;
		}
	})
}

// User authentication
/*
Business Logic
1. Check the database if the user email exists
2. Compare the password provided in the login form with the password stored in the database
3. Generate or return a JSON web token if the user is successfully logged in and return false if not
*/

module.exports.loginUser = (reqBody) =>{
	return User.findOne( {email: reqBody.email})
	.then(result =>{
		// If user does not exist
		if(result === null){
			return false;
		}else{
			//User exists
			// Create a variable "isPasswordCorrect" to return the result of comparing the login form password and the database password
			// "compareSync" method is used to compare a non encrypted password from the login form
				// to the encrypted password retrieved from the database and returns "true" or "false" value depending on the result
			
			// A good practice for boolean variable or constants is to use the word "is" or "are" 
				// at the beginning in the form of is+Noun
					// eg. isDone, isAdmin, areFinish, etc 
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
					// reqBody.password = ilalagay ng user sa form
					// result.password = iccompare yung data sa database which is encrypted
				// If the password match/result o the above code is true
				// companreSync method is used to compare encrypted data sa data sa login form
			if(isPasswordCorrect){
				// Generate an access token
				// use the "createAccessToken" method (dahil gumamit na tayo ng module) defined in the "auth.js" file
				// returning an object back to the front-end
				// We will be using the mongoose method "toObject"
					// it converts to mongoose object into a plain javascript object
				return { accessToken: auth.createAccessToken(result.toObject()) }
				// accessToken = pwede sya irename
				// false kapag walang toObject()
					// dahil galing sya sa mongoose object (yung data parameter wc uses model) wc is hindi mababasa ng user
			}else{
				// Passwords do not match
				return false;
			}
		}	

	})
}

// Start of Activity
module.exports.getProfile = (data) =>{
	return User.findById(data.userId)
	// return User.findById( {_id: userId.id} ) is also possible
	// return User.findById( { data.userId )
	.then(result => {
		if(result === null){
			return false;
		}else{
			result.password = ""
			// pwede result.password = undefined
			return result;
		}
	})
}

module.exports.allUsers = ()=>{
	return User.find({})
	.then(result =>{
		return result
	})
}

// Enroll a user to a class or course
/*
Business Logic
1. Find the document and the database using the user'ss ID
2. Add the course ID to the user's enrollment array 
3. To save the data in the database 
4. Find the document in the database using the course's ID
5. Add the user ID to the course's enrollees array
6. Save the data in the database
7. Error handling
	7.1 If successfulk, return true
	7.2 Else, return false
*/

// async await will be used in enrolling the user because we will need to update
	// This is used for MULTIPLE FUNCTIONS like this example below
	// 2 separate documents when enrolling a user
module.exports.enroll = async(data) => {
	// Creates an "isUserUpdated" variable and returns true upon successul update, toherwise, false
	//Using the "await" keyword will allow the enroll method to complete updating the user before
		// returning a response back to the client
	let isUserUpdated = await User.findById(data.userId)	// Ito yung data na nakalagay sa route
	.then(user =>{
		// Add the courseId in the user's enrollments array
		user.enrollments.push({courseId: data.courseId})								// .push para madala ang data sa array
								// We use this kasi req.body natin ito kukunin
									// wc is an object
	// Saves the updated user information in the database
	return user.save()
	.then((user, error)=>{
		if(error){
			return false;
		}else{
			return true;
		}// the isUserUpdated tells us if the course was successfully added to the user
		 // Kailangan muna matapos yung await function para mailagaya ang data sa isUserUpdates
	})							
})

	let isCourseUpdated = await Course.findById(data.courseId) 			// req.body sa routes
	.then(course=>{
		// Add the userid in the course's enrollees array
		course.enrollees.push({userId: data.userId})		// enrolleess and property sa Course.js
	
		return course.save()
		.then((course, error)=>{
			if(error){
				return false;
			}else{
				return true;
			}
		})// The isCourseUpdated tells us if the user was successfully saved to the course
	})	  // Kailangan muna matapos yung await function para mailagaya ang data sa isCourseUpdated

	// Condition that will check if the user and course documents have been successfully updated
	if(isUserUpdated && isCourseUpdated){
		return true;
	}else{
		// if user enrollment fails
		return false;
	}	

}

